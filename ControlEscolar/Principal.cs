﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ControlEscolar
{
    public partial class Principal : Form
    {
        public Principal()
        {
            InitializeComponent();
        }

        private void UsuariosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Frm_Usuarios frm_Usuarios = new Frm_Usuarios();
            frm_Usuarios.ShowDialog();
        }

        private void AlumnosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Frm_Alumnos frm_Alumnos = new Frm_Alumnos();
            frm_Alumnos.ShowDialog();
        }

        private void maestrosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Frm_Maestros frm_Maestros = new Frm_Maestros();
            frm_Maestros.ShowDialog();
        }

        private void escuelasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Frm_Escuelas _Escuelas = new Frm_Escuelas();
            _Escuelas.ShowDialog();
        }

        private void escuelasDosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            EscuelaApp _escuela = new EscuelaApp();
            _escuela.Show();
        }
    }
}
