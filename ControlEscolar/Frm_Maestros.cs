﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LogicaNegocio.ControlEscolar;
using Enitades.ControlEscolar;
using System.IO;

namespace ControlEscolar
{
    public partial class Frm_Maestros : Form
    {
        MaestroManejador _MaestroManejador;
        Maestros _Maestros;
        string directorio = @"C:\ControlEscolar\Archivos\";
        public Frm_Maestros()
        {
            InitializeComponent();
            _MaestroManejador = new MaestroManejador();
            _Maestros = new Maestros();
        }
        private void BuscarMaestros(string filtro)
        {
            Dtg_Datos.DataSource = _MaestroManejador.ObtenerLista(filtro);
        }

        private void Btn_nuevo_Click(object sender, EventArgs e)
        {
            MaestrosModal _maestrosModal = new MaestrosModal();
            _maestrosModal.ShowDialog();
            BuscarMaestros("");
        }

        private void Frm_Maestros_Load(object sender, EventArgs e)
        {
            BuscarMaestros("");
        }

        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            BuscarMaestros(txtBuscar.Text);
        }
        private void Eliminar()
        {
            string id = Dtg_Datos.CurrentRow.Cells["NControl"].Value.ToString();
            _MaestroManejador.Eliminar(id);
        }
        private void EliminarCarpeta()
        {
            directorio +=Dtg_Datos.CurrentRow.Cells["NControl"].Value.ToString();
            if (Directory.Exists(directorio))
            {
                Directory.Delete(directorio,true);
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Estás seguro que deseas eliminar este registro?", "Eliminar registro", MessageBoxButtons.YesNo) == DialogResult.Yes) ;
            {
                try
                {
                    Eliminar();
                    EliminarCarpeta();
                    BuscarMaestros("");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

            }
        }
        private void BindingMaestro()
        {
            _Maestros.NControl = Dtg_Datos.CurrentRow.Cells["NControl"].Value.ToString();
            _Maestros.Nombre = Dtg_Datos.CurrentRow.Cells["Nombre"].Value.ToString();
            _Maestros.Apellidopaterno = Dtg_Datos.CurrentRow.Cells["Apellidopaterno"].Value.ToString();
            _Maestros.Apellidomaterno = Dtg_Datos.CurrentRow.Cells["Apellidomaterno"].Value.ToString();
            _Maestros.FechaNacimiento = Dtg_Datos.CurrentRow.Cells["FechaNacimiento"].Value.ToString();
            _Maestros.Esta = Dtg_Datos.CurrentRow.Cells["Esta"].Value.ToString();
            _Maestros.Municipio = Dtg_Datos.CurrentRow.Cells["Municipio"].Value.ToString();
            _Maestros.Sexo = Dtg_Datos.CurrentRow.Cells["Sexo"].Value.ToString();
            _Maestros.CorreoElectronico = Dtg_Datos.CurrentRow.Cells["CorreoElectronico"].Value.ToString();
            _Maestros.Tarjeta = Dtg_Datos.CurrentRow.Cells["Tarjeta"].Value.ToString();
            _Maestros.DocTitulo = Dtg_Datos.CurrentRow.Cells["DocTitulo"].Value.ToString();
            _Maestros.DocMaestria= Dtg_Datos.CurrentRow.Cells["DocMaestria"].Value.ToString();
            _Maestros.DocDoctorado= Dtg_Datos.CurrentRow.Cells["DocDoctorado"].Value.ToString();
        }

        private void Dtg_Datos_DoubleClick(object sender, EventArgs e)
        {
            BindingMaestro();
            MaestrosModal maestrosModal = new MaestrosModal(_Maestros);
            maestrosModal.ShowDialog();
            BuscarMaestros("");
        }
    }
}
