﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Enitades.ControlEscolar;
using LogicaNegocio.ControlEscolar;

namespace ControlEscolar
{
    public partial class UsuariosModal : Form
    {
        private UsuarioManejador _usuarioManejador;
        private Usuarios _usuarios;
        public UsuariosModal()
        {
            InitializeComponent();
            _usuarioManejador = new UsuarioManejador();
            _usuarios = new Usuarios();
            //BindingUsuario();
        }
        public UsuariosModal(Usuarios usuarios)
        {
            InitializeComponent();
            _usuarioManejador = new UsuarioManejador();
            _usuarios = new Usuarios();
            _usuarios = usuarios;
            BindingUsuarioReload();
        }
        private void UsuariosModal_Load(object sender, EventArgs e)
        {

        }

        private void Btn_Guardar_Click(object sender, EventArgs e)
        {
            BindingUsuario();
            if (ValidarUsuario())
            {
                Guardar();
                this.Close();
            }
        }
        private bool ValidarUsuario()
        {
            var res = _usuarioManejador.EsUsuarioValido(_usuarios);
            if (!res.Item1) //Diferente de true
            {
                MessageBox.Show(res.Item2);
            }
            return res.Item1;
        }
        private void Guardar()
        {
            _usuarioManejador.Guardar(_usuarios);
        }
        private void BindingUsuarioReload()
        {
            Txt_Usuario.Text = _usuarios.Nombre;
            Txt_ApellidoPaterno.Text = _usuarios.ApellidoPaterno;
            Txt_ApellidoMaterno.Text = _usuarios.ApellidoMaterno;
        }
        private void BindingUsuario()
        {
            if (_usuarios.IdUsuario == 0)
            {
                _usuarios.IdUsuario = 0;
                _usuarios.Nombre = Txt_Usuario.Text;
                _usuarios.ApellidoPaterno = Txt_ApellidoPaterno.Text;
                _usuarios.ApellidoMaterno = Txt_ApellidoMaterno.Text;
            }
        }

        private void Txt_Usuario_TextChanged(object sender, EventArgs e)
        {
        }

        private void Txt_ApellidoPaterno_TextChanged(object sender, EventArgs e)
        {
        }

        private void Txt_ApellidoMaterno_TextChanged(object sender, EventArgs e)
        {
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
