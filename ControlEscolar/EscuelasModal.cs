﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Enitades.ControlEscolar;
using System.IO;
using LogicaNegocio.ControlEscolar;

namespace ControlEscolar
{
    public partial class EscuelasModal : Form
    {
        Escuelas _Escuelas;
        EscuelaManejador _EscuelaManejador;
        EstadosManejador _EstadosManejador;
        MunicipiosManejador _municipiosManejador;
        string directorio = @"C:\ControlEscolar\Logos\";
        bool modificado = false;
        public EscuelasModal()
        {
            InitializeComponent();
            _Escuelas = new Escuelas();
            _EscuelaManejador = new EscuelaManejador();
            _EstadosManejador = new EstadosManejador();
            _municipiosManejador = new MunicipiosManejador();
            BuscarEstados("");
        }
        public EscuelasModal(Escuelas escuelas)
        {
            InitializeComponent();
            _Escuelas = new Escuelas();
            _EscuelaManejador = new EscuelaManejador();
            _EstadosManejador = new EstadosManejador();
            _municipiosManejador = new MunicipiosManejador();
            _Escuelas = escuelas;
            BuscarEstados("");
            BindingEscuelaReload();
            GrupoImagen.Enabled = true;
        }
        private void BuscarEstados(string filtro)
        {
            Cmb_Estado.DataSource = _EstadosManejador.ObtenerLista(filtro);
            Cmb_Estado.DisplayMember = "nombre";
            Cmb_Estado.ValueMember = "codigo";
        }

        private void BuscarMunicipios(string filtro)
        {
            Cmb_Municipio.DataSource = _municipiosManejador.ObtenerLista(filtro);
            Cmb_Municipio.DisplayMember = "nombre";
            Cmb_Municipio.ValueMember = "idm";
        }
        private void BindigEscuela()
        {
            if (_Escuelas.Clave == null)
            {
                _Escuelas.Clave = null;
            }
            _Escuelas.Nombre = Txt_Nombre.Text;
            _Escuelas.Domicilio = Txt_Direccion.Text;
            _Escuelas.Nexterior = Txt_NExterior.Text;
            _Escuelas.Estado = Cmb_Estado.Text;
            _Escuelas.Municipio = Cmb_Municipio.Text;
            _Escuelas.Telefono = Txt_Telefono.Text;
            _Escuelas.Email = Txt_Email.Text;
            _Escuelas.Paginaweb = Txt_PaginaWeb.Text;
            _Escuelas.Director = Txt_Director.Text;
            // _Escuelas.Logo = Txt_Logo.Text;
        }
        private void BindingEscuelaReload()
        {
            Txt_Nombre.Text = _Escuelas.Nombre;
            Txt_Direccion.Text = _Escuelas.Domicilio;
            Txt_NExterior.Text = _Escuelas.Nexterior;
            Cmb_Estado.SelectedItem = _Escuelas.Estado;
            Cmb_Municipio.SelectedItem = _Escuelas.Municipio;
            Txt_Telefono.Text = _Escuelas.Telefono;
            Txt_Email.Text = _Escuelas.Email;
            Txt_PaginaWeb.Text = _Escuelas.Paginaweb;
            Txt_Director.Text = _Escuelas.Director;
            if (_Escuelas.Logo != "")
                Txt_Logo.Text = directorio + _Escuelas.Clave + @"\logo.jpg";
        }
        private void Guardar()
        {
            BindigEscuela();
            _EscuelaManejador.Guardar(_Escuelas);
            MessageBox.Show("Datos guardados correctamente");
            this.Close();
        }
        private void ConsultaRegistro()
        {
            if (!Directory.Exists(directorio)) //Crea y verifica que exista la carpeta logos
                Directory.CreateDirectory(directorio);
            if (!Directory.Exists(directorio + _Escuelas.Clave))
                Directory.CreateDirectory(directorio + _Escuelas.Clave); //Crea y verifica que exista la carpeta de la escuela
        }
        private void CopiarLogo(string direccion)
        {
            string cadenaDirectorio;
            if (File.Exists(direccion))
            {
                cadenaDirectorio = directorio + _Escuelas.Clave + @"\logo.jpg";
                //File.Delete(direccion);
                File.Copy(direccion, cadenaDirectorio, true);
                _Escuelas.Logo = "logo.jpg";
            }
            else
                MessageBox.Show("Error, el archivo ha sido removido del directorio.");
        }
        private void EliminarLogo(string direccion)
        {
            if (File.Exists(direccion))
                File.Delete(direccion);
        }
        private bool ValidarEscuela()
        {
            var res = _EscuelaManejador.EsEscuelaValida(_Escuelas);
            if (!res.Item1) //Diferente de true
            {
                MessageBox.Show(res.Item2);
            }
            return res.Item1;
        }
        private void EscuelasModal_Load(object sender, EventArgs e)
        {

        }

        private void Cmb_Estado_SelectedIndexChanged(object sender, EventArgs e)
        {
            BuscarMunicipios("where fkestado='" + Cmb_Estado.SelectedValue + "'");
        }
        void CopiarEliminar()
        {
            string cadena = directorio + _Escuelas.Clave + @"\logo.jpg";
            if (_Escuelas.Logo == "" && Txt_Logo.Text != "")
                CopiarLogo(Txt_Logo.Text);
            else if (_Escuelas.Logo != "" && Txt_Logo.Text != "")
            {
                EliminarLogo(cadena);
                CopiarLogo(Txt_Logo.Text);
            }
            else if (_Escuelas.Logo!=""&& Txt_Logo.Text=="")
                EliminarLogo(cadena);
        }

        private void Btn_Guardar_Click(object sender, EventArgs e)
        {
            if (GrupoImagen.Enabled)
            {
                //Pic_Logo.Image = null;
                ConsultaRegistro();
                CopiarEliminar();
            }
            BindigEscuela();
            if (ValidarEscuela())
            {
                Guardar();
            }
        }

        private void Btn_Cancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Btn_Browse_Click(object sender, EventArgs e)
        {
            modificado = true;
            OpenFileDialog opfd = new OpenFileDialog();
            opfd.Filter = "Imagen tipo(*.jpg)|*.jpg";
            //opfd.Filter = "Imagen tipo(*.png)|*.png";
            if (opfd.ShowDialog() == DialogResult.OK)
                Txt_Logo.Text = opfd.FileName;
        }

        private void Txt_Logo_TextChanged(object sender, EventArgs e)
        {
            if (File.Exists(Txt_Logo.Text))
                Pic_Logo.Image = Image.FromFile(Txt_Logo.Text);
            else Pic_Logo.Image = null;

        }

        private void Btn_Eliminar_Click(object sender, EventArgs e)
        {
            Txt_Logo.Clear();
        }
    }
}
