﻿namespace ControlEscolar
{
    partial class MaestroArchivos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Gpo_Titulo = new System.Windows.Forms.GroupBox();
            this.Btn_EliminarTitulo = new System.Windows.Forms.Button();
            this.Txt_Titulo = new System.Windows.Forms.TextBox();
            this.Btn_Titulo = new System.Windows.Forms.Button();
            this.Gpo_maestria = new System.Windows.Forms.GroupBox();
            this.Btn_EliminarMaestria = new System.Windows.Forms.Button();
            this.Txt_Maestria = new System.Windows.Forms.TextBox();
            this.Btn_Maestria = new System.Windows.Forms.Button();
            this.Gpo_doctorado = new System.Windows.Forms.GroupBox();
            this.Btn_EliminarDoctorado = new System.Windows.Forms.Button();
            this.Txt_Doctorado = new System.Windows.Forms.TextBox();
            this.Btn_Doctorado = new System.Windows.Forms.Button();
            this.Btn_Cancelar = new System.Windows.Forms.Button();
            this.Btn_Guardar = new System.Windows.Forms.Button();
            this.Gpo_Titulo.SuspendLayout();
            this.Gpo_maestria.SuspendLayout();
            this.Gpo_doctorado.SuspendLayout();
            this.SuspendLayout();
            // 
            // Gpo_Titulo
            // 
            this.Gpo_Titulo.Controls.Add(this.Btn_EliminarTitulo);
            this.Gpo_Titulo.Controls.Add(this.Txt_Titulo);
            this.Gpo_Titulo.Controls.Add(this.Btn_Titulo);
            this.Gpo_Titulo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.Gpo_Titulo.ForeColor = System.Drawing.Color.White;
            this.Gpo_Titulo.Location = new System.Drawing.Point(12, 12);
            this.Gpo_Titulo.Name = "Gpo_Titulo";
            this.Gpo_Titulo.Size = new System.Drawing.Size(827, 72);
            this.Gpo_Titulo.TabIndex = 0;
            this.Gpo_Titulo.TabStop = false;
            this.Gpo_Titulo.Text = "Título universitario";
            // 
            // Btn_EliminarTitulo
            // 
            this.Btn_EliminarTitulo.FlatAppearance.BorderSize = 5;
            this.Btn_EliminarTitulo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Btn_EliminarTitulo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.Btn_EliminarTitulo.ForeColor = System.Drawing.Color.White;
            this.Btn_EliminarTitulo.Location = new System.Drawing.Point(648, 20);
            this.Btn_EliminarTitulo.Name = "Btn_EliminarTitulo";
            this.Btn_EliminarTitulo.Size = new System.Drawing.Size(169, 37);
            this.Btn_EliminarTitulo.TabIndex = 19;
            this.Btn_EliminarTitulo.Text = "Eliminar";
            this.Btn_EliminarTitulo.UseVisualStyleBackColor = true;
            this.Btn_EliminarTitulo.Click += new System.EventHandler(this.Btn_EliminarTitulo_Click);
            // 
            // Txt_Titulo
            // 
            this.Txt_Titulo.BackColor = System.Drawing.Color.White;
            this.Txt_Titulo.Enabled = false;
            this.Txt_Titulo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Titulo.Location = new System.Drawing.Point(6, 25);
            this.Txt_Titulo.Name = "Txt_Titulo";
            this.Txt_Titulo.Size = new System.Drawing.Size(461, 26);
            this.Txt_Titulo.TabIndex = 18;
            this.Txt_Titulo.TextChanged += new System.EventHandler(this.Txt_Titulo_TextChanged);
            // 
            // Btn_Titulo
            // 
            this.Btn_Titulo.FlatAppearance.BorderSize = 5;
            this.Btn_Titulo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Btn_Titulo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.Btn_Titulo.ForeColor = System.Drawing.Color.White;
            this.Btn_Titulo.Location = new System.Drawing.Point(473, 20);
            this.Btn_Titulo.Name = "Btn_Titulo";
            this.Btn_Titulo.Size = new System.Drawing.Size(169, 37);
            this.Btn_Titulo.TabIndex = 17;
            this.Btn_Titulo.Text = "Seleccionar archivo";
            this.Btn_Titulo.UseVisualStyleBackColor = true;
            this.Btn_Titulo.Click += new System.EventHandler(this.Btn_Aceptar_Click);
            // 
            // Gpo_maestria
            // 
            this.Gpo_maestria.Controls.Add(this.Btn_EliminarMaestria);
            this.Gpo_maestria.Controls.Add(this.Txt_Maestria);
            this.Gpo_maestria.Controls.Add(this.Btn_Maestria);
            this.Gpo_maestria.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.Gpo_maestria.ForeColor = System.Drawing.Color.White;
            this.Gpo_maestria.Location = new System.Drawing.Point(12, 90);
            this.Gpo_maestria.Name = "Gpo_maestria";
            this.Gpo_maestria.Size = new System.Drawing.Size(827, 72);
            this.Gpo_maestria.TabIndex = 19;
            this.Gpo_maestria.TabStop = false;
            this.Gpo_maestria.Text = "Maestría";
            // 
            // Btn_EliminarMaestria
            // 
            this.Btn_EliminarMaestria.FlatAppearance.BorderSize = 5;
            this.Btn_EliminarMaestria.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Btn_EliminarMaestria.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.Btn_EliminarMaestria.ForeColor = System.Drawing.Color.White;
            this.Btn_EliminarMaestria.Location = new System.Drawing.Point(648, 20);
            this.Btn_EliminarMaestria.Name = "Btn_EliminarMaestria";
            this.Btn_EliminarMaestria.Size = new System.Drawing.Size(169, 37);
            this.Btn_EliminarMaestria.TabIndex = 19;
            this.Btn_EliminarMaestria.Text = "Eliminar";
            this.Btn_EliminarMaestria.UseVisualStyleBackColor = true;
            this.Btn_EliminarMaestria.Click += new System.EventHandler(this.Btn_EliminarMaestria_Click);
            // 
            // Txt_Maestria
            // 
            this.Txt_Maestria.BackColor = System.Drawing.Color.White;
            this.Txt_Maestria.Enabled = false;
            this.Txt_Maestria.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Maestria.Location = new System.Drawing.Point(6, 25);
            this.Txt_Maestria.Name = "Txt_Maestria";
            this.Txt_Maestria.Size = new System.Drawing.Size(461, 26);
            this.Txt_Maestria.TabIndex = 18;
            this.Txt_Maestria.TextChanged += new System.EventHandler(this.Txt_Maestria_TextChanged);
            // 
            // Btn_Maestria
            // 
            this.Btn_Maestria.FlatAppearance.BorderSize = 5;
            this.Btn_Maestria.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Btn_Maestria.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.Btn_Maestria.ForeColor = System.Drawing.Color.White;
            this.Btn_Maestria.Location = new System.Drawing.Point(473, 20);
            this.Btn_Maestria.Name = "Btn_Maestria";
            this.Btn_Maestria.Size = new System.Drawing.Size(169, 37);
            this.Btn_Maestria.TabIndex = 17;
            this.Btn_Maestria.Text = "Seleccionar archivo";
            this.Btn_Maestria.UseVisualStyleBackColor = true;
            this.Btn_Maestria.Click += new System.EventHandler(this.Btn_Maestria_Click);
            // 
            // Gpo_doctorado
            // 
            this.Gpo_doctorado.Controls.Add(this.Btn_EliminarDoctorado);
            this.Gpo_doctorado.Controls.Add(this.Txt_Doctorado);
            this.Gpo_doctorado.Controls.Add(this.Btn_Doctorado);
            this.Gpo_doctorado.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.Gpo_doctorado.ForeColor = System.Drawing.Color.White;
            this.Gpo_doctorado.Location = new System.Drawing.Point(12, 168);
            this.Gpo_doctorado.Name = "Gpo_doctorado";
            this.Gpo_doctorado.Size = new System.Drawing.Size(827, 72);
            this.Gpo_doctorado.TabIndex = 20;
            this.Gpo_doctorado.TabStop = false;
            this.Gpo_doctorado.Text = "Doctorado";
            // 
            // Btn_EliminarDoctorado
            // 
            this.Btn_EliminarDoctorado.FlatAppearance.BorderSize = 5;
            this.Btn_EliminarDoctorado.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Btn_EliminarDoctorado.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.Btn_EliminarDoctorado.ForeColor = System.Drawing.Color.White;
            this.Btn_EliminarDoctorado.Location = new System.Drawing.Point(652, 20);
            this.Btn_EliminarDoctorado.Name = "Btn_EliminarDoctorado";
            this.Btn_EliminarDoctorado.Size = new System.Drawing.Size(169, 37);
            this.Btn_EliminarDoctorado.TabIndex = 20;
            this.Btn_EliminarDoctorado.Text = "Eliminar";
            this.Btn_EliminarDoctorado.UseVisualStyleBackColor = true;
            this.Btn_EliminarDoctorado.Click += new System.EventHandler(this.Btn_EliminarDoctorado_Click);
            // 
            // Txt_Doctorado
            // 
            this.Txt_Doctorado.BackColor = System.Drawing.Color.White;
            this.Txt_Doctorado.Enabled = false;
            this.Txt_Doctorado.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Doctorado.Location = new System.Drawing.Point(6, 25);
            this.Txt_Doctorado.Name = "Txt_Doctorado";
            this.Txt_Doctorado.Size = new System.Drawing.Size(461, 26);
            this.Txt_Doctorado.TabIndex = 18;
            this.Txt_Doctorado.TextChanged += new System.EventHandler(this.Txt_Doctorado_TextChanged);
            // 
            // Btn_Doctorado
            // 
            this.Btn_Doctorado.FlatAppearance.BorderSize = 5;
            this.Btn_Doctorado.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Btn_Doctorado.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.Btn_Doctorado.ForeColor = System.Drawing.Color.White;
            this.Btn_Doctorado.Location = new System.Drawing.Point(473, 20);
            this.Btn_Doctorado.Name = "Btn_Doctorado";
            this.Btn_Doctorado.Size = new System.Drawing.Size(169, 37);
            this.Btn_Doctorado.TabIndex = 17;
            this.Btn_Doctorado.Text = "Seleccionar archivo";
            this.Btn_Doctorado.UseVisualStyleBackColor = true;
            this.Btn_Doctorado.Click += new System.EventHandler(this.Btn_Doctorado_Click);
            // 
            // Btn_Cancelar
            // 
            this.Btn_Cancelar.FlatAppearance.BorderSize = 5;
            this.Btn_Cancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Btn_Cancelar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Cancelar.ForeColor = System.Drawing.Color.White;
            this.Btn_Cancelar.Location = new System.Drawing.Point(723, 243);
            this.Btn_Cancelar.Name = "Btn_Cancelar";
            this.Btn_Cancelar.Size = new System.Drawing.Size(116, 65);
            this.Btn_Cancelar.TabIndex = 20;
            this.Btn_Cancelar.Text = "Cancelar";
            this.Btn_Cancelar.UseVisualStyleBackColor = true;
            this.Btn_Cancelar.Click += new System.EventHandler(this.button3_Click);
            // 
            // Btn_Guardar
            // 
            this.Btn_Guardar.Enabled = false;
            this.Btn_Guardar.FlatAppearance.BorderSize = 5;
            this.Btn_Guardar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Btn_Guardar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Guardar.ForeColor = System.Drawing.Color.White;
            this.Btn_Guardar.Location = new System.Drawing.Point(594, 244);
            this.Btn_Guardar.Name = "Btn_Guardar";
            this.Btn_Guardar.Size = new System.Drawing.Size(116, 62);
            this.Btn_Guardar.TabIndex = 19;
            this.Btn_Guardar.Text = "Guardar";
            this.Btn_Guardar.UseVisualStyleBackColor = true;
            this.Btn_Guardar.Click += new System.EventHandler(this.Btn_Guardar_Click);
            // 
            // MaestroArchivos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Green;
            this.ClientSize = new System.Drawing.Size(848, 320);
            this.ControlBox = false;
            this.Controls.Add(this.Btn_Cancelar);
            this.Controls.Add(this.Gpo_doctorado);
            this.Controls.Add(this.Btn_Guardar);
            this.Controls.Add(this.Gpo_maestria);
            this.Controls.Add(this.Gpo_Titulo);
            this.Name = "MaestroArchivos";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MaestroArchivos";
            this.Load += new System.EventHandler(this.MaestroArchivos_Load);
            this.Gpo_Titulo.ResumeLayout(false);
            this.Gpo_Titulo.PerformLayout();
            this.Gpo_maestria.ResumeLayout(false);
            this.Gpo_maestria.PerformLayout();
            this.Gpo_doctorado.ResumeLayout(false);
            this.Gpo_doctorado.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox Gpo_Titulo;
        private System.Windows.Forms.Button Btn_Titulo;
        private System.Windows.Forms.TextBox Txt_Titulo;
        private System.Windows.Forms.GroupBox Gpo_maestria;
        private System.Windows.Forms.TextBox Txt_Maestria;
        private System.Windows.Forms.Button Btn_Maestria;
        private System.Windows.Forms.GroupBox Gpo_doctorado;
        private System.Windows.Forms.TextBox Txt_Doctorado;
        private System.Windows.Forms.Button Btn_Doctorado;
        private System.Windows.Forms.Button Btn_Cancelar;
        private System.Windows.Forms.Button Btn_Guardar;
        private System.Windows.Forms.Button Btn_EliminarTitulo;
        private System.Windows.Forms.Button Btn_EliminarMaestria;
        private System.Windows.Forms.Button Btn_EliminarDoctorado;
    }
}