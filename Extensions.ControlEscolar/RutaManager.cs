﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Enitades.ControlEscolar;
using System.IO;

namespace Extensions.ControlEscolar
{
    public class RutaManager
    {
        private string _appPath;
        private const string LOGOS = "logos"; //LAS CONSTANTES VAN EN MAYÚSCULAS
        public RutaManager(string appPath)
        {
            _appPath = appPath;
        }
        public string RutaRepositoriosLogos
        {
            get { return Path.Combine(_appPath, LOGOS); }
        }
        public void CrearRepositoriosLogo()
        {
            if (!Directory.Exists(RutaRepositoriosLogos))
            {
                Directory.CreateDirectory(RutaRepositoriosLogos);
            }
        }
        public void CrearRepositoriosEscuela(int escuelaId)
        {
            CrearRepositoriosLogo();
            string ruta = Path.Combine(RutaRepositoriosLogos, escuelaId.ToString());
            if (!Directory.Exists(ruta))
                Directory.CreateDirectory(ruta);

        }
        public string RutaLogoEscuela(EscuelaDos escuela)
        {
            return Path.Combine(RutaRepositoriosLogos, escuela.Id.ToString(), escuela.Logo);
        }
    }
}
