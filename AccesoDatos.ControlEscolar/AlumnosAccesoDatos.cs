﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Enitades.ControlEscolar;
using ConexionBd;
using System.Data;
namespace AccesoDatos.ControlEscolar
{
    public class AlumnosAccesoDatos
    {
        Conexion _conexion;
        public AlumnosAccesoDatos()
        {
            _conexion = new Conexion("localhost", "root", "", "escolar", 3306);
        }
        public void Guardar(Alumnos alumnos)
        {
            if (alumnos.NControl == null)
            {
                alumnos.NControl = GenerarCodigo(alumnos); //Generar control
                string cadena = String.Format("insert into alumno values('{0}','{1}','{2}','{3}','{4}','{5}'," +
                    "'{6}','{7}','{8}','{9}','{10}')", alumnos.NControl, alumnos.Nombre, alumnos.ApellidoPaterno,
                    alumnos.ApellidoMaterno, alumnos.Sexo, alumnos.FechaNacimiento, alumnos.CorreoEletronico,
                    alumnos.Telefono, alumnos.Estado, alumnos.Municipio, alumnos.Domicilio);
                _conexion.EjecutarConsulta(cadena);
            }
            else
            {
                string cadena = String.Format("update alumno set nombre='{0}',apellidopaterno='{1}',apellidomaterno='{2}'," +
               "sexo='{3}',fechaNacimiento='{4}',email='{5}',telefono='{6}',estado='{7}',municipio='{8}'," +
               "domicilio='{9}' where NControl='{10}'", alumnos.Nombre, alumnos.ApellidoPaterno,
               alumnos.ApellidoMaterno, alumnos.Sexo, alumnos.FechaNacimiento, alumnos.CorreoEletronico,
               alumnos.Telefono, alumnos.Estado, alumnos.Municipio, alumnos.Domicilio, alumnos.NControl);
                _conexion.EjecutarConsulta(cadena);
            }
        }
        private string GenerarCodigo(Alumnos alumnos)
        {
            string codigo;
            codigo = alumnos.Nombre.Substring(0, 2);
            codigo += alumnos.ApellidoPaterno[0];
            codigo += alumnos.ApellidoMaterno[0];
            codigo += alumnos.FechaNacimiento.Substring(3, 2);
            codigo += alumnos.FechaNacimiento.Substring(0, 2);
            return codigo;
        }

        public void Eliminar(string nControl)
        {
            string cadena = string.Format("DELETE FROM alumno where NControl='{0}'", nControl);
            _conexion.EjecutarConsulta(cadena);
        }

        public List<Alumnos> ObtenerLista(string filtro)
        {
            var list = new List<Alumnos>();
            string consulta = string.Format("SELECT * FROM alumno where nombre like '%{0}%'", filtro);
            var ds = _conexion.ObtenerDatos(consulta, "alumno");
            var dt = ds.Tables[0];
            foreach (DataRow row in dt.Rows)
            {
                var Alumno = new Alumnos
                {
                    NControl = row["NControl"].ToString(),
                    Nombre = row["Nombre"].ToString(),
                    ApellidoPaterno = row["ApellidoPaterno"].ToString(),
                    ApellidoMaterno = row["ApellidoMaterno"].ToString(),
                    Sexo = row["Sexo"].ToString(),
                    FechaNacimiento = row["FechaNacimiento"].ToString(),
                    CorreoEletronico = row["Email"].ToString(),
                    Telefono = row["Telefono"].ToString(),
                    Estado = row["Estado"].ToString(),
                    Municipio = row["Municipio"].ToString(),
                    Domicilio = row["Domicilio"].ToString(),
                };
                list.Add(Alumno);
            }
            return list;
        }
    }
}
