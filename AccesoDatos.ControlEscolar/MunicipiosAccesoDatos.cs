﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConexionBd;
using Enitades.ControlEscolar;
using System.Data;

namespace AccesoDatos.ControlEscolar
{
    public class MunicipiosAccesoDatos
    {
        Conexion _conexion;
        public MunicipiosAccesoDatos()
        {
            _conexion = new Conexion("localhost", "root", "", "escolar", 3306);
        }
        public List<Municipios> ObtenerLista(string filtro)
        {
            var list = new List<Municipios>();
            string consulta = string.Format("SELECT * FROM municipio {0}", filtro);
            var ds = _conexion.ObtenerDatos(consulta, "municipio");
            var dt = ds.Tables[0];
            foreach (DataRow row in dt.Rows)
            {
                var Municipios = new Municipios
                {
                    Idm = Convert.ToInt32(row["idm"]),
                    Nombre = row["nombre"].ToString(),
                    Fkestado = row["fkestado"].ToString()
                };
                list.Add(Municipios);
            }
            return list;
        }
    }
}
