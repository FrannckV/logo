﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enitades.ControlEscolar
{
    public class Maestros
    {
        private string nControl, nombre, apellidopaterno, apellidomaterno, fechaNacimiento, esta, municipio, sexo, correoElectronico, tarjeta, docTitulo, docMaestria, docDoctorado;
        private string anio;
        public string NControl { get => nControl; set => nControl = value; }
        public string Nombre { get => nombre; set => nombre = value; }
        public string Apellidopaterno { get => apellidopaterno; set => apellidopaterno = value; }
        public string Apellidomaterno { get => apellidomaterno; set => apellidomaterno = value; }
        public string FechaNacimiento { get => fechaNacimiento; set => fechaNacimiento = value; }
        public string Esta { get => esta; set => esta = value; }
        public string Municipio { get => municipio; set => municipio = value; }
        public string Sexo { get => sexo; set => sexo = value; }
        public string CorreoElectronico { get => correoElectronico; set => correoElectronico = value; }
        public string Tarjeta { get => tarjeta; set => tarjeta = value; }
        public string DocTitulo { get => docTitulo; set => docTitulo = value; }
        public string DocMaestria { get => docMaestria; set => docMaestria = value; }
        public string DocDoctorado { get => docDoctorado; set => docDoctorado = value; }
        public string Anio { get => anio; set => anio = value; }
    }
}
