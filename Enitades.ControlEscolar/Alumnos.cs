﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enitades.ControlEscolar
{
    public class Alumnos
    {
        private string _nControl, _nombre, _apellidoPaterno, _apellidoMaterno,_sexo, _fechaNacimiento,_correoEletronico,_telefono, _estado, _municipio, _domicilio; //Agregar los que falta, Ctrl + r + e get y set
        public string NControl { get => _nControl; set => _nControl = value; }
        public string Nombre { get => _nombre; set => _nombre = value; }
        public string ApellidoPaterno { get => _apellidoPaterno; set => _apellidoPaterno = value; }
        public string ApellidoMaterno { get => _apellidoMaterno; set => _apellidoMaterno = value; }
        public string Sexo { get => _sexo; set => _sexo = value; }
        public string FechaNacimiento { get => _fechaNacimiento; set => _fechaNacimiento = value; }
        public string CorreoEletronico { get => _correoEletronico; set => _correoEletronico = value; }
        public string Telefono { get => _telefono; set => _telefono = value; }
        public string Estado { get => _estado; set => _estado = value; }
        public string Municipio { get => _municipio; set => _municipio = value; }
        public string Domicilio { get => _domicilio; set => _domicilio = value; }
    }
}
