﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enitades.ControlEscolar
{
    public class Escuelas
    {
        string clave, nombre, domicilio, nexterior, estado, municipio, telefono, email, paginaweb, director, logo;

        public string Clave { get => clave; set => clave = value; }
        public string Nombre { get => nombre; set => nombre = value; }
        public string Domicilio { get => domicilio; set => domicilio = value; }
        public string Nexterior { get => nexterior; set => nexterior = value; }
        public string Estado { get => estado; set => estado = value; }
        public string Municipio { get => municipio; set => municipio = value; }
        public string Telefono { get => telefono; set => telefono = value; }
        public string Email { get => email; set => email = value; }
        public string Paginaweb { get => paginaweb; set => paginaweb = value; }
        public string Director { get => director; set => director = value; }
        public string Logo { get => logo; set => logo = value; }
    }
}
