﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enitades.ControlEscolar
{
    public class Usuarios //Agregar el public
    {
        private int _idUsuario; //Todos los atributos, métodos, etc privados cominezan con guiòn bajo.
        private string _nombre;
        private string _apellidoPaterno;
        private string _apellidoMaterno;
        // Ctrl + r + e + enter para crear método get y set
        //Ctrl + k + d organiza el código de acuerdo al estándar
        public int IdUsuario { get => _idUsuario; set => _idUsuario = value; }
        public string Nombre { get => _nombre; set => _nombre = value; }
        public string ApellidoPaterno { get => _apellidoPaterno; set => _apellidoPaterno = value; }
        public string ApellidoMaterno { get => _apellidoMaterno; set => _apellidoMaterno = value; }
    }
}
