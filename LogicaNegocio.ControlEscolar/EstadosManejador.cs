﻿using System.Collections.Generic;
using AccesoDatos.ControlEscolar;
using Enitades.ControlEscolar;

namespace LogicaNegocio.ControlEscolar
{
    public class EstadosManejador
    {
        private EstadosAccesoDatos _estadosAccesoDatos;
        public EstadosManejador()
        {
            _estadosAccesoDatos = new EstadosAccesoDatos(); 
        }
        public List<Estados> ObtenerLista(string filtro)
        {
            var list = new List<Estados>();
            list = _estadosAccesoDatos.ObtenerLista(filtro);
            return list;
        }
    }
}
